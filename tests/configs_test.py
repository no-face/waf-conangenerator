#!/usr/bin/env python
# -*- coding: utf-8 -*-

from waf_generator.WafConfig import WafConfig

from .utils.fakes import *
from conans.model.build_info import DepsCppInfo

import pytest

class TestConfigs(object):
    def setup_method(self, method):
        s = Steps()

        self.given = s
        self.when = s
        self.then = s


    @pytest.mark.parametrize("compiler", ["gcc", "clang"])
    def test_architecture_flags(self, compiler):
        self.given.settings_with(arch="x86", compiler=compiler)
        self.when.creating_a_WafConfig_instance()
        self.then.attribute___should_contain__('cflags'  , '-m32')
        self.then.attribute___should_contain__('cxxflags', '-m32')
        self.then.attribute___should_contain__('ldflags' , '-m32')

    @pytest.mark.parametrize("lib", ["libstdc++", "libstdc++11", "libc++"])
    def test_stdlib_flags_in_clang(self, lib):
        compiler = "clang"
        lib_flag = '-stdlib='+ (lib if lib != "libstdc++11" else "libstdc++")

        self.given.settings_with(compiler=compiler)
        self.given.conanfile.settings.compiler.libcxx = lib

        self.when.creating_a_WafConfig_instance()
        self.then.attribute___should_contain__('cxxflags', lib_flag)
        self.then.attribute___should_contain__('ldflags' , lib_flag)

###########################################################################

class Steps(object):

    def __init__(self):
        self.waf_config = None

        self.conanfile = FakeConanFile()
        self.deps_build_info = DepsCppInfo()

    def settings_with(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self.conanfile.settings, key, value)

    def creating_a_WafConfig_instance(self):
        self.waf_config = WafConfig(self.conanfile, self.deps_build_info)

    def attribute___should_contain__(self, attr_name, expected_value):
        assert expected_value in getattr(self.waf_config, attr_name)
