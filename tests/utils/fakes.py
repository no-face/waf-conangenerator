#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.model.build_info import DepsCppInfo, CppInfo
from conans.model.settings import Settings
from conans.model.options import PackageOptions, Options

import os

from unittest.mock import MagicMock


SETTINGS_DEFINITION = {
    "compiler" : {
        "gcc" : {
            "libcxx" : ["libstdc++", "libstdc++11"]
        },
        "clang" : {
            "libcxx" : ["libstdc++", "libstdc++11", "libc++"]
        }
    },
    "arch" : ["x86", "x86_64"]
}

class FakeConanFile(object):
    def __init__(self, settings_def=SETTINGS_DEFINITION):
        self.deps_env_info  = {}
        self.env_info       = {}
        self.deps_cpp_info  = DepsCppInfo()
        self.cpp_info       = {}
        self.deps_user_info = {}
        self.output         = MagicMock()

        self.settings = Settings(definition=settings_def)
        self.options  = Options(PackageOptions.loads("{}"))


class Store(object):
    '''Class used to allow store arbitrary properties'''

    def append_value(self, key, values):
        if not hasattr(self, key):
            setattr(self, key, [])

        getattr(self, key).extend(values)

class FakeContext(object):
    def __init__(self):
        self.env = Store()

class FakedCppInfo(CppInfo):
    def __init__(self, ref_name, root_folder, **kwargs):
        super(FakedCppInfo,self).__init__(ref_name, root_folder, **kwargs)

    def _filter_paths(self, paths):
        abs_paths = [os.path.join(self.rootpath, p)
                    if not os.path.isabs(p) else p for p in paths]
        return abs_paths
