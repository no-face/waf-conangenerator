#!/usr/bin/env python
# -*- coding: utf-8 -*-

from waf_generator import WafGenerator
from waf_generator.WafConfig import WafConfig

from .utils.fakes import *

from conans import ConanFile
from conans.model.build_info import DepsCppInfo, CppInfo
from conans.model.settings import Settings

import os
from os import path
import tempfile

try:
    UNICODE_EXISTS = bool(type(unicode))
except NameError:
    unicode = str

def build_cpp_info(ref_name, base_dir, **kw):
    cpp_info = FakedCppInfo(ref_name, base_dir)

    for attr, value in kw.items():
        setattr(cpp_info, attr, value)

    return cpp_info

class TestGenerator:
    SYSROOT_PATH="/path/to/sysroot"

    deps = {
        'foo' : build_cpp_info('foo', 'foo_dir',
                libs = ["foo"]
        ),
        'bar' : build_cpp_info('bar', 'bar_dir',
                libs = ["bar"],
                cppflags = ["-std=c++11"]
        ),
        'my-awesome-lib' : build_cpp_info('my-awesome-lib', 'awesomeroot',
                libs = ["my-awesome"]
        ),
        'empty' : build_cpp_info('empty', 'empty',
            libdirs = [],
            bindirs = [],
            includedirs = []
        ),
        'toolchain': build_cpp_info('toolchain', 'toolchain_dir',
            sysroot = SYSROOT_PATH,
        ),
    }

    def setup_method(self, method):
        self.conanfile = FakeConanFile()

        self.gen = WafGenerator(self.conanfile)

        self.generated_content = None

        self.content_scope = dict()

    def teardown_method(self, method):
        pass

    def test_default_CppInfo(self):
        cpp_info = build_cpp_info("pkg_name", "base_dir")

        assert cpp_info.bindirs     == ["bin"]
        assert cpp_info.libdirs     == ["lib"]
        assert cpp_info.includedirs == ["include"]

        assert cpp_info.include_paths == [path.join("base_dir", "include")]
        assert cpp_info.lib_paths     == [path.join("base_dir", "lib")]

    def test_content_generation(self):
        self.given_dependencies('bar', 'foo', 'empty')

        self.when_the_content_is_generated()

        self.then_the_content_should_be(
            args = ["bar_dir" + os.sep, "foo_dir" + os.sep],
            lines = [
                'def configure(conf):',
                    '\t# bar',
                    '\tconf.env.CXXFLAGS_bar=["-std=c++11"]',
                    '\tconf.env.INCLUDES_bar=["{0}include"]',
                    '\tconf.env.LIB_bar=["bar"]',
                    '\tconf.env.LIBPATH_bar=["{0}lib"]',
                    '\tconf.env.RPATH_bar=["{0}lib"]',
                    '',
                    '\t# foo',
                    '\tconf.env.INCLUDES_foo=["{1}include"]',
                    '\tconf.env.LIB_foo=["foo"]',
                    '\tconf.env.LIBPATH_foo=["{1}lib"]',
                    '\tconf.env.RPATH_foo=["{1}lib"]',
                    '',
                    '\tconf.env.conan_dependencies = ["bar", "foo"]',
                    ''
            ]
        )

    def test_gen_content_for_empty_dependencies(self):
        self.given_dependencies('empty')

        self.when_the_content_is_generated()

        self.then_the_content_should_be(
            lines = [
                'def configure(conf):',
                    '\tpass'
            ]
        )

    def test_generated_content_should_contain_a_configure_function(self):
        self.given_dependencies('bar', 'my-awesome-lib', 'empty')

        self.when_the_content_is_generated_and_evaluated()

        self.then_a_configure_function_should_be_generated()

    def test_content_should_include_dependencies_properties(self):
        self.given_dependencies('bar', 'my-awesome-lib')

        self.when_the_content_is_generated()
        self.when_configure_function_is_executed()

        assert self.ctx is not None
        self.then_env_should_contain_properties(
            {
                'CXXFLAGS_bar'              : ["-std=c++11"],
                'INCLUDES_bar'              : [path.join("bar_dir", "include")],
                'LIB_bar'                   : ["bar"],
                'LIBPATH_bar'               : [path.join("bar_dir", "lib")],
                'RPATH_bar'                 : [path.join("bar_dir", "lib")],

                'INCLUDES_my_awesome_lib'   : [path.join("awesomeroot", "include")],
                'LIB_my_awesome_lib'        : ["my-awesome"],
                'LIBPATH_my_awesome_lib'    : [path.join("awesomeroot", "lib")],
                'RPATH_my_awesome_lib'      : [path.join("awesomeroot", "lib")],
            }
        )

    def test__content_should_include_use_list(self):
        self.given_dependencies('bar', 'my-awesome-lib', 'empty')

        self.when_the_content_is_generated()
        self.when_configure_function_is_executed()

        self.then_env_should_contain_properties(
            {
                'conan_dependencies' : ["bar", "my_awesome_lib"] #empty is not included
            }
        )

    def test_generator_should_include_the_stdlib_flags(self):
        self.given_a_conanfile_with_settings({
            "compiler" : "gcc",
            "compiler.libcxx" : "libstdc++"
        })

        self.when_the_content_is_generated()
        self.when_configure_function_is_executed()

        self.then_env_should_contain_properties(
            {
                "DEFINES" : ["_GLIBCXX_USE_CXX11_ABI=0"],
            }
        )


    def test_generator_should_include_the_stdlib_flags_for_clang(self):
        self.given_a_conanfile_with_settings({
            "compiler" : "clang",
            "compiler.libcxx" : "libstdc++11"
        })

        if not self.conanfile.settings.compiler:
            print("no compiler")

        self.when_the_content_is_generated()
        self.when_configure_function_is_executed()

        self.then_env_should_contain_properties(
            {
                "DEFINES" : ["_GLIBCXX_USE_CXX11_ABI=1"],
                "CXXFLAGS": ["-stdlib=libstdc++"]
            }
        )

    def test_generator_should_add_syspath_to_flags(self):
        self.given_a_conanfile_with_settings({
            "compiler" : "gcc",
            "compiler.libcxx" : "libstdc++"
        })

        self.given_dependencies('toolchain')

        self.when_the_content_is_generated()
        self.when_configure_function_is_executed()

        self.then_env_should_contain_properties(
            {
                "CFLAGS"   : ["--sysroot={}".format(self.SYSROOT_PATH)],
                "CXXFLAGS" : ["--sysroot={}".format(self.SYSROOT_PATH)],
                "LDFLAGS"  : ["--sysroot={}".format(self.SYSROOT_PATH)],
            }
        )

    ########################################################################

    def given_dependencies(self, *deps):
        for d in deps:
            self.add_dependency(d, self.deps[d])

    def given_a_conanfile_with_settings(self, settings):
        values_list = []

        for name in sorted(settings.keys()):
            values_list.append((name, settings[name]))

        self.conanfile.settings.values_list = values_list

    def when_the_content_is_generated(self):
        self.generated_content = self.gen.content

    def when_the_content_is_generated_and_evaluated(self):
        self.when_the_content_is_generated()
        self.evaluate_content()

    def when_configure_function_is_executed(self):
        self.evaluate_content()
        self.run_configure()

    def then_a_configure_function_should_be_generated(self):
        assert callable(self.content_scope["configure"])

    def then_env_should_contain_properties(self, properties):
        self.object_properties_should_contain_values(self.ctx.env, properties)

    def object_properties_should_contain_values(self, obj, properties):
        for name, values in properties.items():
            assert hasattr(obj, name)

            obj_values = getattr(obj, name)

            for expected_value in values:
                assert expected_value in obj_values

    def object_should_contain_properties(self, obj, properties):
        for name, value in properties.items():
            assert hasattr(obj, name)
            assert getattr(obj, name) == value

    def then_the_content_should_be(self, args=[], lines=[]):
        #expected_out='\n'.join(lines).format(*args)

        expected_lines = [unicode(l.format(*args)) for l in lines]

        content_lines = self.generated_content.split('\n')

        print("generated lines:\n{}".format("\n".join(content_lines)))
        print("expected lines:\n{}".format("\n".join(expected_lines)))

        assert content_lines == expected_lines
        #for i in range(len(content_lines)):
        #    assert content_lines[i] == lines[i], "Line {0} = '{1}'".format(i, content_lines[i])

    ##################################################################################

    def evaluate_content(self):
        exec(self.generated_content, self.content_scope)

    def run_configure(self):
        self.ctx = FakeContext()

        self.content_scope["configure"](self.ctx)

    def add_dependency(self, dep_name, deps):
        self.gen.deps_build_info.add(dep_name, deps)
