#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
from os import path

class ExampleConanFile(ConanFile):
    build_policy = "missing"
    settings = "os", "compiler", "build_type", "arch"

    generators = "Waf"

    build_requires = (
        "waf/0.1.1@noface/stable"
    )

    exports = "wscript"

    @property
    def build_path(self):
        return path.join(self.build_folder, 'build')

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.run("waf configure build -o {}".format(self.build_path), cwd=self.source_folder)

    def test(self):
        self.run(path.join(self.build_path, "example"))
