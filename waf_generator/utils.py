#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

PY2 = sys.version_info[0] == 2

if PY2:
    string_types = basestring,
else:
    string_types = str,

def is_string(value):
    return isinstance(value, string_types)

def is_iterable(value):
    try:
        iter(value)
        return True
    except TypeError:
        return False
