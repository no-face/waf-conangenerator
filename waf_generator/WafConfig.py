#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .flags import stdlib_flags, stdlib_defines

from conans import AutoToolsBuildEnvironment

from collections import OrderedDict

NAMES_MAP = {
    'include_paths' : 'INCLUDES',
    'lib_paths'     : 'LIBPATH',
    'bin_paths'     : '',
    'libs'          : 'LIB',
    'defines'       : 'DEFINES',
    'cflags'        : 'CFLAGS',
    'cppflags'      : 'CXXFLAGS',
    'sharedlinkflags' : 'LINKFLAGS',
    'exelinkflags' : '',
}

class WafConfig(object):
    def __init__(self, conanfile, deps_build_info):
        self.gen_rpaths = True # should be configurable

        self.deps_info = OrderedDict()

        # global flags
        self.defines  = set()
        self.cxxflags = set()
        self.cflags   = set()
        self.ldflags  = set()

        self.add_global_flags(conanfile, deps_build_info)
        self.add_dependencies(deps_build_info)

    def keys(self):
        return self.deps_info.keys()

    def items(self):
        return self.deps_info.items()

    def add_global_flags(self, conanfile, deps):
        self._add_compiler_global_flags(conanfile, deps)
        self._add_autotools_flags(conanfile, deps)

    def add_dependencies(self, deps_build_info):
        for depName in deps_build_info.deps:
            dep = deps_build_info[depName]
            self.add_dependency(depName, dep)

    def add_dependency(self, depName, dep):
        self.deps_info[depName] = OrderedDict()

        for attr in NAMES_MAP:
            self.add_property(attr, depName, dep)

        if self.gen_rpaths and dep.lib_paths:
            #prop = [self.makePropertyName('RPATH', depName), dep.lib_paths]
            prop = [('RPATH', depName), dep.lib_paths]
            self.addProp(depName, prop)

    def add_property(self, attr, depName, dep):
        prop = self.makeProperty(attr, depName, getattr(dep, attr))

        self.addProp(depName, prop)

    def addProp(self, depName, prop):
        if prop:
            self.deps_info[depName][prop[0]] = prop[1]

    def makeProperty(self, prop, depName, depValue):
        if not NAMES_MAP[prop] or not depValue:
            return None

        #propName = self.makePropertyName(self.NAMES_MAP[prop], depName)
        propName = (NAMES_MAP[prop], depName)

        return (propName, depValue)

    def makePropertyName(self, propName, dependencyName):
        return propName + '_' + self.fix_dependency_name(dependencyName)

    def fix_dependency_name(self, name):
        '''Replaces special characters in dependencies'''

        # Currently only replaces '-' characters
        return name.replace('-', '_')


    def _add_compiler_global_flags(self, conanfile, deps):
        comp = conanfile.settings.get_safe("compiler")

        if comp:
            # Extends content of flags with 'stdlib' related flags
            self.defines.update(stdlib_defines(conanfile))

            libcxx_flags = stdlib_flags(conanfile)
            self.cxxflags.update(libcxx_flags)
            self.ldflags.update(libcxx_flags)

            if deps and deps.sysroot and comp in ["gcc", "clang"]:
                sysroot_opt = "--sysroot={}".format(deps.sysroot)

                self.cflags.add(sysroot_opt)
                self.cxxflags.add(sysroot_opt)
                self.ldflags.add(sysroot_opt)

    def _add_autotools_flags(self, conanfile, deps):
        autot = AutoToolsBuildEnvironment(conanfile)

        # Does not include flags in 'deps', they are included by each dependency

        self._add_flags_to(autot.flags    , self.cflags  , exclude=deps.cflags)
        self._add_flags_to(autot.flags    , self.cxxflags, exclude=deps.cflags)
        self._add_flags_to(autot.cxx_flags, self.cxxflags, exclude=deps.cppflags)
        self._add_flags_to(autot.defines  , self.defines , exclude=deps.defines)

        deps_ldflags = deps.sharedlinkflags + deps.exelinkflags
        self._add_flags_to(autot.link_flags, self.ldflags, exclude=deps_ldflags)

    def _add_flags_to(self, flags, dest, exclude):
        for f in flags:
            if f and (f not in exclude):
                dest.add(f)
