#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

class ConfigureWriter(object):

    def __init__(self, out=None):

        self.out = out or StringIO()

    def write_configure(self, wafConfigs):
        self.out.write(u"def configure(conf):\n")

        counter = self.write_global_properties(wafConfigs)
        counter += self.write_dependencies(wafConfigs)

        if not counter: # No configuration writed
            self.out.write(u"\tpass")
        else:
            self.write_uses(wafConfigs)

    def write_global_properties(self, wafConfigs):
        counter = 0

        for attr_name in ["defines", "cxxflags", "cflags", "ldflags"]:
            attr = getattr(wafConfigs, attr_name, None)

            if attr:
                counter += 1
                line = u'\tconf.env.append_value("{}", {})\n'.format(attr_name.upper(), self.gen_list(attr))

                self.out.write(line)

        if counter:
            self.out.write(u'\n')

        return counter

    def write_dependencies(self, wafConfigs):
        counter = 0
        if wafConfigs:
            for depName, deps in wafConfigs.items():
                counter += self.write_dep(depName, deps)

        return counter

    def write_uses(self, wafConfigs):
        '''write one variable containing the dependencies names that a target
           should include into 'use' property
        '''

        deps_names = [self.fix_dependency_name(name) for name, value in wafConfigs.items() if value ]

        self.out.write(u"\tconf.env.conan_dependencies = {0}\n".format(self.gen_list(deps_names)))

    def write_dep(self, depName, depProperties):
        if not depProperties: #empty
            return 0

        self.out.write(u"\t# %s\n" % depName)

        for key, value in sorted(depProperties.items()):
            prop_name = self.makePropertyName(key[0], key[1])

            self.out.write(u"\tconf.env.{0}={1}\n".format(prop_name, self.gen_list(value)))

        self.out.write(u'\n')

        return 1

    def gen_list(self, list):
        quotedList = [('"'+ v + '"') for v in list]

        out = StringIO()
        out.write(u'[')
        out.write(u", ".join(quotedList))
        out.write(u']')

        return out.getvalue()

    def makePropertyName(self, propName, dependencyName):
        return propName + '_' + self.fix_dependency_name(dependencyName)

    def fix_dependency_name(self, name):
        '''Replaces special characters in dependencies'''

        # Currently only replaces '-' characters
        return name.replace('-', '_')
