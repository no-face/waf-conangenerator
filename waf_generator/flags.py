#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import utils

try:
    # From conan 1.3.3

    from conans.client.build.compiler_flags import libcxx_flag, libcxx_define

    def _to_iterable(value):
        if not value:
            return []
        if utils.is_string(value) or not utils.is_iterable(value):
            return  [value]

        # iterable
        return value

    def stdlib_flags(conanfile):
        flags = libcxx_flag(conanfile.settings)
        return _to_iterable(flags)


    def stdlib_defines(conanfile):
        flags = libcxx_define(conanfile.settings)
        return _to_iterable(flags)

except:
    try:
        from conans.client.build.autotools_environment import stdlib_flags, stdlib_defines
    except:
        from conans.client.configure_build_environment import stdlib_flags, stdlib_defines
