#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.model import Generator

from .WafConfig import WafConfig
from .ConfigureWriter import ConfigureWriter

class WafGenerator(Generator):
    def __init__(self, conanfile):
        super(WafGenerator, self).__init__(conanfile)

        self.conanfile = conanfile
        self.gen_rpaths = True

    @property
    def filename(self):
        return "conanbuildinfo_waf.py"

    @property
    def content(self):
        configs = WafConfig(self.conanfile, self.deps_build_info)

        writer = ConfigureWriter()
        writer.write_configure(configs)

        return writer.out.getvalue()


